# Introduction
Screengress is a tool that will take a screenshot of the intel map at regular intervals for your operations, while also removing the interface for optimal Intel map view.

## What is not screengress ?
This is not a scraper.
It does not make any more requests that the intel map does in a normal view usage, nor does it alter the requests.
The data received by this app does not get out of the app in any other form than screenshots.

# Installation
You should have [node.js 8 or more](https://nodejs.org/en/download/) installed with npm, and use the command line ```npm install``` to setup the dependencies.
# Usage
The first thing is getting the location coordinates to look at. For this go to the [Intel map](https://ingress.com/intel), zoom-in to the location you want to screenshot, click the *Link* icon in the top right corner of the Intel map and copy this link address.
You can then start screengress by using the command line :
```node main.js "https://www.ingress.com/intel?ll=13,37&z=42"``` (replace this address by the one you copied just before).
A chromium window will open and you will have to login to your Google account, then the Intel will open and screenshoting will start.
You can add arguments to change the default behaviour :
* -o or --output *folder* : sets the screenshot destination folder. Default is the current date + latitude/longitude
* -w or --width *number* : screenshot width in pixels, defaults to 1024
* -h or --height *number* : screenshot height in pixels, defaults to 1024
* -i or --interval *number* : interval in seconds between each screenshot, defaults to 60
* -u or --user-data *folder* : chromium session path. it will store login cookies and intel map session, so with this argument you only need to login once
* --headless : will not show the chromium window. you will still need to login to the intel map, so use it coupled with --user-data argument
# Disclaimer
Ingress name and data belongs to Niantic.
Screengress is not afiliated with Niantic.  Use at your own risks.