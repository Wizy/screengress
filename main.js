const puppeteer = require('puppeteer');
const fs = require('fs');
const URL = require('url');
const querystring = require('querystring');
const dayjs = require('dayjs');
const argv = require('minimist')(process.argv.slice(2));

const device = {
    name: 'Desktop 1920x1080',
    userAgent: 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36',
    viewport: {
        width: 1024,
        height: 1024,
    }
};

let location,
    outputDirectory,
    headless,
    width,
    height,
    interval,
    userDataDir;

function usage() {
    console.log(`${process.argv.slice(0, 2)} [options...] "<intel map location url>"`);
    console.log(`Open https://www.ingress.com/intel on your browser, and click on "Link" to get the intel map location url`);
    console.log(`Options:`);
    console.log(` -o, --output         Screenshots path`);
    console.log(`     --headless       Hide chromium window`);
    console.log(` -w  --width          Screenshot width (default 1024)`);
    console.log(` -h  --height         Screenshot height (default 1024)`);
    console.log(` -i  --interval       Interval in seconds between screenshots (default 60s)`);
    console.log(` -u, --user-data      Path to chromium session, for persistent auth`);
}

function parseLocationFromIntelLink(intelMapUrl) {
    const url = URL.parse(intelMapUrl);
    if(url.hostname !== 'www.ingress.com' && url.hostname !== 'ingress.com') {
        console.error('Intel URL required');
        return false;
    }

    const query = querystring.parse(url.query);
    if(!('ll' in query) || !('z' in query)) {
        console.error('Missing location information in URL. Use the "Link" icon on the Intel map.');
        return false;
    }

    const z = parseFloat(query.z);
    if(isNaN(z) || z < 1 || z > 22) {
        console.error('Invalid zoom level');
        return false;
    }

    const [latitude, longitude] = query.ll.split(',').map(parseFloat);
    if(isNaN(latitude) || isNaN(longitude) || latitude < -90 || latitude > 90 || longitude < -180 || longitude > 180) {
        console.error('Invalid latitude and/or longitude');
        return false;
    }

    return {latitude, longitude, z};
}

function parseArguments() {
    if(argv._.length !== 1)
        return false;

    location = parseLocationFromIntelLink(argv._[0]);
    outputDirectory = argv.o || `${dayjs().format('YYYYMMDD')}-${location.latitude},${location.longitude},${location.z}z`;
    headless = 'headless' in argv;
    device.viewport.width = width = argv.width || argv.w || 1024;
    device.viewport.height = height = argv.height || argv.h || 1024;
    interval = argv.interval || argv.i || 60;
    userDataDir = argv['user-data'] || argv.u;
    hidePortals = argv['hide-portals'];
    hideLinks = argv['hide-links'];
    hideFields = argv['hide-fields'];

    return true;
}



async function hideIntelInterface(page) {
    await page.addStyleTag({
        content: `body {overflow: hidden; }
#header, #player_stats, #game_stats, #geotools, #comm, #filters_container, #portal_filter_header, #snapcontrol, img[src^="https://maps.gstatic.com/mapfiles/"], .gmnoprint, button, .gm-style-cc { display: none; }
#dashboard_container { left: 0; right: 0; bottom: 0; top: 0; border: none; }`,
    });
}

(async () => {
    if(!parseArguments(argv)) {
        usage();
        process.exit(1);
    }

    try {
        fs.mkdirSync(outputDirectory);
    }
    catch(e) {
    }

    const browser = await puppeteer.launch({
        headless,
        userDataDir,
    });
    const [page] = await browser.pages();
    await page.bringToFront();
    await page.emulate(device);
    page.on('close', () => {
        console.log('closing');
        browser.close();
    });

    const url = `https://www.ingress.com/intel?ll=${location.latitude},${location.longitude}&z=${location.z}`;
    await page.goto(url);
    console.log('please login');

    await page.waitForSelector('#map_canvas', {visible: true, timeout: 0});
    console.log('intel map is loaded');

    await hideIntelInterface(page);

    async function doScreenshot() {
        await page.waitForSelector('#loading_msg', {hidden: true, timeout: 0});

        const date = dayjs().format('YYYY-MM-DDTHH-mm-ss');
        await page.screenshot({path: `${outputDirectory}/${date}.png`});
        console.log(`[${date}] screenshot`);

        setTimeout(async () => {
            links = new Set();
            await page.reload();
            await hideIntelInterface(page);
            doScreenshot();
        }, interval * 1000);
    }
    doScreenshot();
})();
